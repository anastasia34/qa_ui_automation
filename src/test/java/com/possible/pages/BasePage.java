package com.possible.pages;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.possible.utilities.Driver;

import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public abstract class BasePage {

    public BasePage(){
        PageFactory.initElements(new AppiumFieldDecorator(Driver.getDriver()), this);
    }

    protected static WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 120);
    
}
