package com.possible.pages;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class CardDashboard extends BasePage{

    @AndroidFindBy(xpath = "//XCUIElementTypeStaticText[@name='Current Balance']")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Current Balance']")
    @FindBy(xpath = "//XCUIElementTypeStaticText[@name='Current Balance']")
    private MobileElement currentBalanceLbl;

    @AndroidFindBy(xpath = "//XCUIElementTypeOther[@name='Pending Transactions']")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Pending Transactions']")
    @FindBy(xpath = "//XCUIElementTypeOther[@name='Pending Transactions']")
    private MobileElement pendingTransactionLbl;

    @AndroidFindBy(xpath = "//XCUIElementTypeOther[@name='Pending Payment']")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Pending Payment']")
    @FindBy(xpath = "//XCUIElementTypeOther[@name='Pending Payment']")
    private MobileElement pendingPaymentLbl;

    @AndroidFindBy(xpath = "//XCUIElementTypeOther[@name='Available To Spend']")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Available To Spend']")
    @FindBy(xpath = "//XCUIElementTypeOther[@name='Available To Spend']")
    private MobileElement availableToSpendLbl;

    @AndroidFindBy(accessibility = "Next Payment")
    @iOSXCUITFindBy(accessibility = "Next Payment")
    @FindBy(xpath = "//XCUIElementTypeStaticText[@name='Next Payment']")
    private MobileElement nextPaymentLbl;

    @AndroidFindBy(xpath = "//XCUIElementTypeStaticText[@name='Transactions']")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Transactions']")
    @FindBy(xpath = "//XCUIElementTypeStaticText[@name='Transactions']")
    private MobileElement transactionsLbl;

    @AndroidFindBy(xpath = "//XCUIElementTypeStaticText[@name='Got Your Possible Card?']")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Got Your Possible Card?']")
    @FindBy(xpath = "//XCUIElementTypeStaticText[@name='Got Your Possible Card?']")
    private MobileElement physicalCardActivationLbl;

    @AndroidFindBy(xpath = "//XCUIElementTypeStaticText[@name='Great! Let's get your card activated and ready to use.']")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Great! Let's get your card activated and ready to use.']")
    @FindBy(xpath = "//XCUIElementTypeStaticText[@name='Great! Let's get your card activated and ready to use.']")
    private MobileElement physicalCardActivationText;

    @AndroidFindBy(xpath = "//XCUIElementTypeOther[@name='Activate Your Card']")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Activate Your Card']")
    @FindBy(xpath = "//XCUIElementTypeOther[@name='Activate Your Card']")
    private MobileElement physicalCardActivationBtn;

    @AndroidFindBy(xpath = "//XCUIElementTypeStaticText[@name='Next Payment']")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Next Payment']")
    @FindBy(xpath = "//XCUIElementTypeStaticText[@name='Next Payment']")
    private MobileElement nextPaymentDate;

    @AndroidFindBy(xpath = "//XCUIElementTypeOther[@name='$686.99 Current Balance']")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='$686.99 Current Balance']")
    @FindBy(xpath = "//XCUIElementTypeOther[@name='$686.99 Current Balance']")
    private MobileElement currentBalance;

    @AndroidFindBy(xpath = "(//XCUIElementTypeOther[@name='Show Virtual Card'])[1]")
    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name='Show Virtual Card'])[1]")
    @FindBy(xpath = "(//XCUIElementTypeOther[@name='Show Virtual Card'])[1]")
    private MobileElement showVirtualCardBtn;

    @AndroidFindBy(xpath = "//XCUIElementTypeOther[@name='Pay Now']")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Pay Now']")
    @FindBy(xpath = "//XCUIElementTypeOther[@name='Pay Now']")
    private MobileElement payNowBtn;

    @AndroidFindBy(xpath = "//XCUIElementTypeOther[@name='Pay Over Time']")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Pay Over Time']")
    @FindBy(xpath = "//XCUIElementTypeOther[@name='Pay Over Time']")
    private MobileElement payOverTimeBtn;

    public MobileElement getCurrentBalance(){
        return currentBalance;
    }
    
    public void verifyDashboardDisplayed(){
        System.out.print("\nCard dashboard is displayed");
        wait.until(ExpectedConditions.visibilityOf(currentBalance));
        //assertTrue(payNowBtn.isDisplayed());
        // assertTrue(payOverTimeBtn.isDisplayed());
        assertTrue(showVirtualCardBtn.isDisplayed());
        assertTrue(pendingTransactionLbl.isDisplayed());
        assertTrue(pendingPaymentLbl.isDisplayed());
        assertTrue(availableToSpendLbl.isDisplayed());
        System.out.print("..✓");
    
    }
}
