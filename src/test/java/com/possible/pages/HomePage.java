package com.possible.pages;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class HomePage extends BasePage{

    // @AndroidFindBy(id = "btn_create_account") 
    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name='btn_create_account'])[1]")
    @FindBy(xpath = "(//XCUIElementTypeOther[@name='btn_create_account'])[1]")
    public MobileElement getStartedBtn;

    // @AndroidFindBy(xpath = "//XCUIElementTypeButton[@name='LOGIN']")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='LOGIN']")
    @FindBy(xpath = "//XCUIElementTypeButton[@name='LOGIN']")
    private MobileElement loginBtn;

    public void createNewAccount(){
        getStartedBtn.click();
    }
    
    public void clickLogin(){
        wait.until(ExpectedConditions.visibilityOf(loginBtn));
        loginBtn.click();
    }

    public void clickGetStarted(){
        wait.until(ExpectedConditions.visibilityOf(getStartedBtn));
        getStartedBtn.click();
    }
    
}
