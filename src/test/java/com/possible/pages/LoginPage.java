package com.possible.pages;

import org.openqa.selenium.support.ui.ExpectedConditions;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.support.FindBy;


public class LoginPage extends BasePage {

    HomePage homePage = new HomePage();

 
    @AndroidFindBy(xpath = "//XCUIElementTypeStaticText[@name='label_welcome']")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='label_welcome']")
    @FindBy(xpath = "//XCUIElementTypeStaticText[@name='label_welcome']")
    private MobileElement welcomeBackTitle;

    @AndroidFindBy(xpath = "(//XCUIElementTypeOther[@name='Welcome back'])[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]")
    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name='Welcome back'])[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]")
    @FindBy(xpath = "(//XCUIElementTypeOther[@name='Welcome back'])[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]")
    private MobileElement logo;

    @AndroidFindBy(xpath = "(//XCUIElementTypeOther[@name='App Version: 2.1.19 | UI Version: v390'])[3]")
    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name='App Version: 2.1.19 | UI Version: v390'])[3]")
    @FindBy(xpath = "(//XCUIElementTypeOther[@name='App Version: 2.1.19 | UI Version: v390'])[3]")
    private MobileElement appVersion;

    @AndroidFindBy(xpath = "(//XCUIElementTypeOther[@name='Email input_email'])[3]")
    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name='Email input_email'])[3]")
    @FindBy(xpath = "(//XCUIElementTypeOther[@name='Email input_email'])[3]")
    private MobileElement emailInputField;

    @AndroidFindBy(xpath = "(//XCUIElementTypeOther[@name='Password input_password'])[3]")
    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name='Password input_password'])[3]")
    @FindBy(xpath = "(//XCUIElementTypeOther[@name='Password input_password'])[3]")
    private MobileElement passwordInputField;

    @AndroidFindBy(xpath = "(//XCUIElementTypeOther[@name='Login'])[1]")
    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name='Login'])[1]")
    @FindBy(xpath = "(//XCUIElementTypeOther[@name='Login'])[1]")
    private MobileElement loginBtn;

    @AndroidFindBy(xpath = "//XCUIElementTypeStaticText[@name='label_sign_up']")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='label_sign_up']")
    @FindBy(xpath = "//XCUIElementTypeStaticText[@name='label_sign_up']")
    private MobileElement signUpBtn;

    @AndroidFindBy(xpath = "//XCUIElementTypeStaticText[@name='By logging in or signing up, you agree to Possible Finance's Terms of Service, Privacy Policy, Electronic Communications Consent and Coastal Bank's Privacy Policy']")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='By logging in or signing up, you agree to Possible Finance's Terms of Service, Privacy Policy, Electronic Communications Consent and Coastal Bank's Privacy Policy']")
    @FindBy(xpath = "//XCUIElementTypeStaticText[@name='By logging in or signing up, you agree to Possible Finance's Terms of Service, Privacy Policy, Electronic Communications Consent and Coastal Bank's Privacy Policy']")
    private MobileElement agreementMsg;

    public void moveToDevMode(){
        
        homePage.clickLogin();

        boolean elementExists = true;

        wait.until(ExpectedConditions.visibilityOf(appVersion));

        while(elementExists){
            try {
                appVersion.click();
            } catch ( org.openqa.selenium.NoSuchElementException e) {
                elementExists = false;
            }
        }
    }

    public void login(String email, String password){
        System.out.print("\nCard user login");
        homePage.clickLogin();
        wait.until(ExpectedConditions.visibilityOf(welcomeBackTitle));

        emailInputField.click();
        emailInputField.sendKeys(email);

        passwordInputField.click();
        passwordInputField.sendKeys(password);

        wait.until(ExpectedConditions.elementToBeClickable(loginBtn));
        loginBtn.click();
        System.out.print("..✓");
    }
}
