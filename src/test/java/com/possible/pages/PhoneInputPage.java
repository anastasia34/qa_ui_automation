package com.possible.pages;

import org.openqa.selenium.support.ui.ExpectedConditions;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.support.FindBy;

public class PhoneInputPage extends BasePage{

    @AndroidFindBy(xpath = "")
    @iOSXCUITFindBy(xpath = "")
    @FindBy(xpath = "")
    private MobileElement phoneInputField;

    public void inputPhoneNumber(String phoneNumber){
        
        phoneInputField.sendKeys(phoneNumber);

    }


}
