package com.possible.pages;

import org.openqa.selenium.support.ui.ExpectedConditions;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.support.FindBy;

public class SignUpPage extends BasePage{

    @AndroidFindBy(xpath = "//XCUIElementTypeStaticText[@name='label_welcome']")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='label_welcome']")
    @FindBy(xpath = "//XCUIElementTypeStaticText[@name='label_welcome']")
    private MobileElement letsGetStartedTitle;

    @AndroidFindBy(xpath = "(//XCUIElementTypeOther[@name='Email input_email'])[3]")
    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name='Email input_email'])[3]")
    @FindBy(xpath = "(//XCUIElementTypeOther[@name='Email input_email'])[3]")
    private MobileElement emailInputField;

    @AndroidFindBy(xpath = "(//XCUIElementTypeOther[@name='Password input_password'])[1]")
    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name='Password input_password'])[1]")
    @FindBy(xpath = "(//XCUIElementTypeOther[@name='Password input_password'])[1]")
    private MobileElement passwordInputField;

    @AndroidFindBy(xpath = "(//XCUIElementTypeOther[@name='Create Account'])[1]")
    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name='Create Account'])[1]")
    @FindBy(xpath = "(//XCUIElementTypeOther[@name='Create Account'])[1]")
    private MobileElement createAccountBtn;

    HomePage homePage = new HomePage();


    public void userSignUp(String email, String password){
       
        System.out.print("\nCard user signup");
        homePage.clickGetStarted();
        wait.until(ExpectedConditions.visibilityOf(letsGetStartedTitle));

        emailInputField.click();
        emailInputField.sendKeys(email);

        passwordInputField.click();
        passwordInputField.sendKeys(password);

        //wait.until(ExpectedConditions.elementToBeClickable(createAccountBtn));
        createAccountBtn.click();
        System.out.print("..✓");
    }
    
}
