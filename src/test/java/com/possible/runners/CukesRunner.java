package com.possible.runners;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(

plugin = {"html:target/cucumber-report.html",
        "json:target/cucumber.json",
        "rerun:target/rerun.txt"
},
features = "src/test/resources/features",
glue = "com/possible/step_definition",
dryRun = false,
tags = "@SmokeTests"
)


public class CukesRunner {
}
