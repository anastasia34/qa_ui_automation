package com.possible.step_definition;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.possible.pages.CardDashboard;
import com.possible.pages.LoginPage;
import com.possible.pages.PhoneInputPage;
import com.possible.pages.SignUpPage;
import com.possible.utilities.ConfigurationReader;
import com.possible.utilities.TestUser;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class CardLogin {

    @Given("Card user logs in")
    public void card_user_login(){
        
        LoginPage loginPage = new LoginPage();
        loginPage.login(ConfigurationReader.getProperty("card_email"), ConfigurationReader.getProperty("password"));

    }

    @Then("Card dashboard is displayed")
    public void card_dashboard_displayed(){
        CardDashboard cardDashboard = new CardDashboard();
        cardDashboard.verifyDashboardDisplayed();
    }

    @Given("User SignUp")
    public void user_signup() {
       SignUpPage signUpPage = new SignUpPage();
       TestUser user = new TestUser();
       PhoneInputPage phoneInputPage = new PhoneInputPage();

       signUpPage.userSignUp(ConfigurationReader.getProperty("card_email"), ConfigurationReader.getProperty("password"));

       phoneInputPage.inputPhoneNumber(user.getPhone());

       
    }
    
}
