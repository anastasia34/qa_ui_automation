package com.possible.step_definition;



import com.possible.pages.LoginPage;
import com.possible.utilities.Driver;

import io.cucumber.java.AfterAll;
import io.cucumber.java.BeforeAll;

public class Hooks {
    
    @BeforeAll
    public static void before_all(){
        System.out.print("\nBefore all");
        LoginPage loginPage = new LoginPage();

        loginPage.moveToDevMode();

    }

    @AfterAll
    public static void after_all(){
        System.out.print("\nAfter all");

        Driver.quitDriver();
    }
}
