package com.possible.utilities;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.JavascriptExecutor;


import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class Driver {
    
    private Driver(){}

    public static String userName = System.getenv("LT_USERNAME") == null ? ConfigurationReader.getProperty("LT_USERNAME")  //Add username here
    : System.getenv("LT_USERNAME");
    public static String accessKey = System.getenv("LT_ACCESS_KEY") == null ? ConfigurationReader.getProperty("LT_ACCESS_KEY") //Add accessKey here
    : System.getenv("LT_ACCESS_KEY");

    private static AppiumDriver<?> driver;
    
    public static AppiumDriver<?> getDriver(){ 
        String deviceName = ConfigurationReader.getProperty("deviceName");       
        try{
        if (driver == null){
            switch (deviceName){
                case "ios":
                    DesiredCapabilities caps = new DesiredCapabilities();
                    caps.setCapability("platformVersion", "15");
                    caps.setCapability("deviceName", "iPhone 12");
                    caps.setCapability("isRealMobile", true);
                    caps.setCapability("app", ConfigurationReader.getProperty("app_url")); //Enter your app url
                    caps.setCapability("platformName", "iOS");
                    caps.setCapability("build", "Java Vanilla - iOS");
                    caps.setCapability("name", "Sample Test Java");
                    caps.setCapability("devicelog", true);
                    caps.setCapability("network", true);
    
    
                    driver = new IOSDriver<>(new URL("https://" + userName + ":" + accessKey + "@beta-hub.lambdatest.com/wd/hub"), caps);
                    break;
                case "android":
                    DesiredCapabilities capabilities = new DesiredCapabilities();
                    capabilities.setCapability("deviceName", "Galaxy S20");
                    capabilities.setCapability("platformVersion", "11");
                    capabilities.setCapability("platformName", "Android");
                    capabilities.setCapability("isRealMobile", true);
                    capabilities.setCapability("app", ConfigurationReader.getProperty("app_url")); //Enter your app url
                    capabilities.setCapability("deviceOrientation", "PORTRAIT");
                    capabilities.setCapability("build", "Java Vanilla - Android");
                    capabilities.setCapability("name", "Sample Test Java");
                    capabilities.setCapability("console", true);
                    capabilities.setCapability("network", false);
                    capabilities.setCapability("visual", true);
                    capabilities.setCapability("devicelog", true);

                    driver = new AndroidDriver<>(new URL("https://" +userName + ":" + accessKey + "@mobile-hub.lambdatest.com/wd/hub"), capabilities);
                    break;
                default:
                throw new RuntimeException("Wrong device name");
            }
            driver.setSetting("snapshotMaxDepth", 59);
            driver.setSetting("customSnapshotTimeout", 50000);
        }
    }catch (MalformedURLException a) {
        ((JavascriptExecutor) driver).executeScript("lambda-status=failed");
        a.printStackTrace();
    }
    return driver;
    }

    public static void quitDriver(){
        if(driver!=null){
            driver.quit();
            driver = null;
        }
    }
}
