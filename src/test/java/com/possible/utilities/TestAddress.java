package com.possible.utilities;

public class TestAddress {

    private String street;
	private String city;
	private String zipcode;

    public void setAddress(String state) {

        switch (state) {
            case "GA":  
                this.street = "2445 Mellville Ave";
                this.city = "Decatur";
                this.zipcode = "30032";
                    break;
            case "PA": 
                this.street = "133 Irene Dr";
                this.city = "Meridian";
                this.zipcode = "16001";
                    break;
            case "NH": 
                this.street = "11 Ivy Ct";
                this.city = "Laconia";
                this.zipcode = "03246";
                    break;
            case "CT":  
                this.street = "22 Boivin St";
                this.city = "Bristol";
                this.zipcode = "06010";
                    break;
            case "SD": 
                this.street = "102 Washington Ave";
                this.city = "Murdo";
                this.zipcode = "57559";
                    break;
            case "MN": 
                this.street = "921 4th St SW";
                this.city = "Willmar";
                this.zipcode = "56201";
                    break;
            case "NM": 
                this.street = "1821 Hopewell St";
                this.city = "Santa Fe";
                this.zipcode = "87505";
                    break;
            case "CO": 
                this.street = "3747 Meade St";
                this.city = "Denver";
                this.zipcode = "80211";
                    break;
            case "WY": 
                this.street = "980 Cliff St";
                this.city = "Lander";
                this.zipcode = "82520";
                    break;
            case "NE": 
                this.street = "105 Corrothers Ave";
                this.city = "Whitman";
                this.zipcode = "69366";
                    break;
            case "NY": 
                this.street = "5 Adela Pl";
                this.city = "Valhalla";
                this.zipcode = "10595";
                    break;
            case "WI": 
                this.street = "115 Knox St";
                this.city = "Wausau";
                this.zipcode = "54401";
                    break;
            case "NC": 
                this.street = "114 Boyd St";
                this.city = "Cary";
                this.zipcode = "27513";
                    break;
            case "AZ": 
                this.street = "7957 W Hatcher Rd";
                this.city = "Peoria";
                this.zipcode = "85345";
                    break;
            case "AL": 
                this.street = "544 E 12th Ave";
                this.city = "Anchorage";
                this.zipcode = "99501";
                    break;
            case "VA": 
                this.street = "2401 Vandover Rd";
                this.city = "Richmond";
                this.zipcode = "23229";
                    break;
            case "AK": 
                this.street = "1506 S Porter St";
                this.city = "Stuttgart";
                this.zipcode = "72160";
                    break;
            case "MT": 
                this.street = "842 7th St";
                this.city = "Havre";
                this.zipcode = "59501";
                    break;
            case "IA": 
                this.street = "3015 Woodland St";
                this.city = "Ames";
                this.zipcode = "50014";
                    break;
            case "OK": 
                this.street = "1326 N Davis St";
                this.city = "Enid";
                this.zipcode = "73701";
                    break;
            case "WA": 
                this.street = "120 13th Ave E";
                this.city = "Seattle";
                this.zipcode = "98102";
                    break;
            case "MO": 
                this.street = "106 Bicknell St";
                this.city = "Columbia";
                this.zipcode = "65203";
                    break;         
            case "DE": 
                this.street = "20 Patriot Dr";
                this.city = "Dover";
                this.zipcode = "19904";
                    break;
            case "IN": 
                this.street = "1320 W Jackson St";
                this.city = "Kokomo";
                this.zipcode = "46901";
                    break;
            case "MI": 
                this.street = "53 Nash St";
                this.city = "Sparta";
                this.zipcode = "49345";
                    break;
            case "RI": 
                this.street = "12 Chatham Rd";
                this.city = "Cranston";
                this.zipcode = "02920";
                    break; 
            case "TX": 
                this.street = "300 Conet Dr";
                this.city = "Odessa";
                this.zipcode = "79763";
                    break;         
            case "KY": 
                this.street = "107 Keenesway Ln";
                this.city = "Versailles";
                this.zipcode = "40383";
                    break; 
            case "UT": 
                this.street = "115 S 300 W St";
                this.city = "Beaver";
                this.zipcode = "84713";
                    break; 
            case "KS": 
                this.street = "2917 Hall St";
                this.city = "Hays";
                this.zipcode = "67601";
                    break;
            case "MS": 
                this.street = "660 N Jefferson St";
                this.city = "Jackson";
                this.zipcode = "39202";
                    break; 
            case "ID": 
                this.street = "3155 3370 W";
                this.city = "Moore";
                this.zipcode = "83255";
                    break;  
            case "TN": 
                this.street = "248 Welch Rd";
                this.city = "Nashville";
                this.zipcode = "37211";
                    break;
            case "HI": 
                this.street = "2726 Aolani Pl";
                this.city = "Honolulu";
                this.zipcode = "96822";
                    break;
            case "IL": 
                this.street = "803 E William St";
                this.city = "Decatur";
                this.zipcode = "62521";
                    break;         
            case "ME": 
                this.street = "24 Sherman Ave";
                this.city = "Bangor";
                this.zipcode = "04401";
                    break;
            case "MD": 
                this.street = "6046 Florey Rd";
                this.city = "Hanover";
                this.zipcode = "21076";
                    break;
            case "NV": 
                this.street = "406 Lindsay Ln";
                this.city = "Gabbs";
                this.zipcode = "89409";
                    break;
            case "NJ": 
                this.street = "138 Market St";
                this.city = "Newark";
                this.zipcode = "07102";
                    break; 
            case "ND": 
                this.street = "414 1st St SW";
                this.city = "Stanley";
                this.zipcode = "58784";
                    break;         
            case "OR": 
                this.street = "35 SE Yew Ln";
                this.city = "Bend";
                this.zipcode = "97702";
                    break; 
            case "VT": 
                this.street = "32 Foster St";
                this.city = "Barre";
                this.zipcode = "05641";
                    break; 
            case "CA": 
                this.street = "704 E McKinley Ave";
                this.city = "Fresno";
                this.zipcode = "93728";
                    break;
            case "OH": 
                this.street = "390 Patten St";
                this.city = "Marion";
                this.zipcode = "43302";
                    break; 
            case "LA": 
                this.street = "1456 Louisiana St";
                this.city = "Jena";
                this.zipcode = "71342";
                    break;  
                default: 
                     break;
        }
	}

    public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	
	@Override
	public String toString(){
		return getStreet() + ", "+getCity()+", "+getZipcode();
	}
    
}
