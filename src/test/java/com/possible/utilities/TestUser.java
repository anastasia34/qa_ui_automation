package com.possible.utilities;

import com.github.javafaker.Faker;

public class TestUser {
    private String firstName;
	private String lastName;
    private String middleName;
    private String phoneNumber; 

    Faker faker = new Faker();

    public String getFirstName(){
        this.firstName = faker.name().firstName();
        return firstName;
    }

    public String getLastName(){
        this.lastName = faker.name().lastName();
        return lastName;
    }

    public String getMiddleName(){
        this.middleName = faker.name().firstName();
        return middleName;
    }

    public String getPhone(){
        this.phoneNumber = faker.phoneNumber().toString();
        return phoneNumber;
    }
}
