# @LoanSignUp
# Feature: Loan SignUp Tests

# Scenario: New user for Possible Loan in KY on waitlist
# Given I am from KY
# And I am a new possible user
# When I signup or login to my possible account
# Then I should see the waitlist page for possible loan


# Scenario Outline: New User for Possible Loan in <state> can apply and accept loan with ACH
# Given I am from <state>
# When I signup for a loan
# And I accept the approved loan
# Then I should be able to get a loan
# Example:
# |state|
# |AL|
# |DE|
# |FL|
# |IA|
# |IN|
# |KS|
# |KY|
# |MI|
# |MS|
# |OK|
# |RI|
# |SC|
# |TN|
# |TX|


# Scenario Outline: Signup & Accept loan via direct from loan supported <state>
# Given I am from <state>
# When I signup for a loan
# And I accept the approved loan
# Then I should be able to get a loan
# Example:
# |state|
# |CA|
# |ID|
# |LA|
# |OH|
# |UT|
# |WA|


# Scenario: New user for Possible Loan in MI with PO Box Address
# Given that I signup as a new Possible Loan user
# And I input my address as a PO Box
# When I submit my address
# Then Possible will return an error message for invalid address


