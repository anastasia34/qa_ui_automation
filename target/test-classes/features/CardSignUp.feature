# @SignUPCard
# Feature: Cards SignUp Tests

# Scenario Outline: Signup for a card from a non-loan Card eligible state
# Given I am included in the Possible Card experiment
# And I am from <state>
# When I apply for a card in the possible app
# And I choose Autopay
# Then I should be able to apply for a possible card
# And I should be able to activate my virtual card
# Example:
#      |state|
#      |  GA |
#      |  PA |
#      |  NH |
#      |  CN |
#      |  SD |
#      |  MN |
#      |  NM |
#      |  CO |
#      |  WY |
#      |  NB |
#      |  NY |
#      |  WI |
#      |  NC |
#      |  AZ |
#      |  AL |
#      |  VA |
#      |  AK |


# Scenario Outline: Signup for a card from a loan state Card eligible
# Given I am included in the Possible Card experiment
# And I am from <state>
# When I apply for a card in the possible app
# And I choose Autopay
# Then I should be able to apply for a possible card
# And I should be able to activate my virtual card
# Example:
#      |state|
#      |  IA |
#      |  OK |
#      |  WA |
#      |  MO |
#      |  AL |
#      |  DE |
#      |  IN |
#      |  MI |
#      |  RI |
#      |  TX |
#      |  KY |
#      |  UT |
#      |  KS |
#      |  MS |
#      |  ID |
#      |  TN |  


# Scenario Outline: Signup for a card from a non-loan Card eligible state
# Given I am included in the Possible Card experiment
# And I am from <state>
# When I apply for a card in the possible app
# And And I choose Manual
# Then I should be able to apply for a possible card
# And I should be able to activate my virtual card
# Example:
#      |state|
#      |  GA |
#      |  PA |
#      |  NH |
#      |  CN |
#      |  SD |
#      |  MN |
#      |  NM |
#      |  CO |
#      |  WY |
#      |  NB |
#      |  NY |
#      |  WI |
#      |  NC |
#      |  AZ |
#      |  AL |
#      |  VA |
#      |  AK |


# Scenario Outline: Signup for a card from a loan state Card eligible
# Given I am included in the Possible Card experiment
# And I am from <state>
# When I apply for a card in the possible app
# And And I choose Manual
# Then I should be able to apply for a possible card
# And I should be able to activate my virtual card
# Example:
#      |state|
#      |  IA |
#      |  OK |
#      |  WA |
#      |  MO |
#      |  AL |
#      |  DE |
#      |  IN |
#      |  MI |
#      |  RI |
#      |  TX |
#      |  KY |
#      |  UT |
#      |  KS |
#      |  MS |
#      |  ID |
#      |  TN |  


# Scenario Outline: Signup for a card from a loan state Not Card eligible
# Given I am not part of the Possible Card experiment
# And I am from <state>
# When I login to my possible account
# Then I should see the loan application dashboard
# Example:
#      |state|
#      |  IA |
#      |  OK |
#      |  WA |
#      |  MO |
#      |  AL |
#      |  DE |
#      |  IN |
#      |  MI |
#      |  RI |
#      |  TX |
#      |  KY |
#      |  UT |
#      |  KS |
#      |  MS |
#      |  ID |
#      |  TN |  


# Scenario: Signup for a card with an active loan Card eligible
# Given I have an active loan
# And I am part of the Possible Card experiment
# When I login to my possible account
# Then I should see my active loan dashboard
# And I should not see the card dashboard


# Scenario: Signup for a card with a paid off loan
# Given I have a paid off loan
# And I am part of the Possible Card experiment
# When I login to my possible account
# And I apply for a possible card
# And I choose Autopay
# Then I should be able to apply for a card
# And I should be able to activate my possible card
# And I should not be able to apply for a loan


# Scenario: Signup for a card from a non-loan state Not Card eligible
# Given I am not part of the Possible Card experiment
# And I am from <state>
# When I signup for possible finance
# Then I should see the loan waitlist
# And I should not see the card application
# Example:
#      |state|
#      |HI|
#      |IL|
#      |ME|
#      |MD|
#      |NV|
#      |NJ|
#      |ND|
#      |OR|
#      |VT|
#      |CA|
#      |OH|
#      |LA|

# Scenario: Login with invalid account
# Given I am a valid Possible Finance card user
# When I login to my card account using invalid credentials
# Then I should not be able to login to the card account
 